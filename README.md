
# Lista asioista, jotka pitää tehdä jotta saat kehitysympäristön pystyyn.

https://jira.valve.fi/wiki/display/rcms3/Sovelluskehitys


# Build-prosessi lyhyesti

Buildaus käyttää projektipohjassa Gulp task runneria, mutta se voi käytännössä olla mitä vain, kunhan package.json sisältää tarvittavat script-määritykset start, build-release (postinstall on valinnainen, riippuen onko muita package managereita, joiden install pitäisi ajaa)

```
{
   "scripts": { 
       "start": "wake-import gulp watch",
       "build-release": "grunt gulp-release",
   }
}
```

Devaus tapahtuu DEVillä ajamalla projektin juuressa käskyn ud (joka ajaa yarn install sekä start). Jotta tiedostot menisivät palvelimelle niin watch task pitää ajaa wake-importin kautta esim:
```
Gulpilla "wake-import gulp watch" 
Webpackilla "wake-import webpack --watch"
<jokin muu> "wake-import <jokin muu> <jotain parametreja>"
```

Käännökset luodaan projektiin ajamalla yarn build-locales. Luodut käännökset commitataan ja pushataan repoon, jotta deployment osaa sitten ne hakea. Jos koodista ei löydy yhtäkään {‰ trans ‰}Käännös{% endtrans %} -tyylistä merkkijonoa, kaatuu wake-locales vastaavanlaiseen virheeseen, 
jonka ohi pääsee luomalla jotain käännöksiä tai jättää komennon ajamatta
```
wake-locales: No translations found or potx-cli.php is missing... { Error: ENOENT: no such file or directory, stat 'templates/general.pot'...
```