
var colors = require('ansi-colors');
var concat = require('gulp-concat');
var globSass = require('gulp-sass-globbing');
var gulp = require('gulp');
var env = require('minimist');
var log = require('fancy-log');
var merge = require('merge-stream');
var noop = require('through2');
var replace = require('gulp-replace');
var rsync = require('gulp-rsync');
var sass = require('gulp-sass');
var size = require('gulp-size');
var uglify = require('gulp-uglify');
var wait = require('gulp-wait');

// Paths
var paths = {
	build: 'build',
	assets: [ 
		'assets/**/*', 
		'!assets/js/**/*',
		'!assets/css/**/*',
	],
	locale: [ 'locale/**/*' ],
	js: [
		'js/*.js',
		'js/**/*.js'
	],
	twig: [ 'templates/**/*.twig' ],
	wakeCustom: [ 'custom/**/*.php' ],
	sass: [ 'sass/**/*.scss', '!sass/**/_globbing*.scss' ],
	manifest: 'manifest*xml',

	script: 'code.js',
	css: 'main.css',
	vendorJs: 'vendor.js',
	vendorCss: 'vendor.css',

	dest: {
		script: 'build/assets/js/',
		css: 'build/assets/css/',
	},

	cwd: process.cwd() + '/'
};

var isProduction = true;
var sassStyle = 'compressed';

if(env.dev === true) {
    sassStyle = 'expanded';
    isProduction = false;
}

// List of concatenated vendor files
var vendorFiles = {
	scripts: [
		'node_modules/angular/angular.min.js',
		'node_modules/angular-*/*.min.js',
	]
};

// Rsync all assets to the build folder
gulp.task('assets', function() {
	return gulp.src(paths.assets).pipe(rsync({
		destination: paths.build, delete: true, archive: true
	}));
});

// Rsync all templates to the build folder
gulp.task('templates', function() {
	return gulp.src(paths.twig).pipe(rsync({
		destination: paths.build, delete: true, archive: true
	}));
});

// Rsync all models and controllers to the build folder
gulp.task('wake_custom', function() {
	return gulp.src(paths.wakeCustom).pipe(rsync({
		destination: paths.build, delete: true, archive: true
	}));
});

// Rsync manifests to the build folder
gulp.task('manifest', function() {
	return gulp.src(paths.manifest).pipe(rsync({
		destination: paths.build, delete: true, archive: true
	}));
});

// Build vendor script file and main script file and save them to the build folder
gulp.task('js', function() {
	return merge(
		// Vendor
		gulp.src(vendorFiles.scripts)
			.pipe(concat(paths.vendorJs))
			.pipe(gulp.dest(paths.dest.script))
			.pipe(size({ title: 'File size', showFiles: true }))
		,
		// Scripts
		gulp.src(paths.js)
			.pipe(concat(paths.script))
			.pipe(gulp.dest(paths.dest.script))
			.pipe(isProduction ? uglify() : noop())
			.pipe(size({ title: 'File size', showFiles: true }))
			.pipe(gulp.dest(paths.dest.script))
	)
});

// Compile sass files to css and save it to the build folder
gulp.task('sass', function () {
	return merge(
		// Create globbing files for faster rebuild
		gulp.src('sass/utility/**/*.scss')
			.pipe(globSass({path: '_globbing-utility.scss'}))
			.pipe(gulp.dest('sass'))
		,	
		gulp.src('sass/partials/base/**/*.scss')
			.pipe(globSass({path: '_globbing-base.scss'}))
			.pipe(gulp.dest('sass/partials'))
		,
		gulp.src('sass/partials/layouts/**/*.scss')
			.pipe(globSass({path: '_globbing-layouts.scss'}))
			.pipe(gulp.dest('sass/partials'))
		,
		gulp.src('sass/partials/blocks/**/*.scss')
			.pipe(globSass({path: '_globbing-blocks.scss'}))
			.pipe(gulp.dest('sass/partials'))
		,
		// Build main css
		gulp.src(paths.sass)
			.pipe(wait(0))
			.pipe(sass({ outputStyle: sassStyle }).on('error', sass.logError))
			.pipe(gulp.dest(paths.dest.css))
			.pipe(size({ title: 'File size', showFiles: true }))
	);
});

// Log watch events to the console
var logChangeEvent = function(evt) {
    log('File', colors.green(evt.path.replace(paths.cwd, '')), 'was', colors.magenta(evt.type));
};

var allTasks = ['manifest', 'js', 'sass', 'wake_custom', 'assets', 'templates'];

gulp.task('watch', allTasks, function () {
	gulp.watch(paths.assets, ['assets']).on('change', logChangeEvent);
	gulp.watch(paths.twig, ['templates']).on('change', logChangeEvent);
	gulp.watch(paths.wakeCustom, ['wake_custom']).on('change', logChangeEvent);
	gulp.watch(paths.manifest, ['manifest']).on('change', logChangeEvent);
	gulp.watch(paths.js, ['js']).on('change', logChangeEvent);
	gulp.watch(paths.sass, ['sass']).on('change', logChangeEvent);
});

gulp.task('default', ['watch']);

gulp.task('build-release', allTasks);