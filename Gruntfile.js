

// KONFIGURAATIO
// ----------------------------------------------------------------------------------------------


var opts = {

	// Build- ja lähdehakemistot.
	// ----------------------------------------------------------------------------------------------

	// build-hakemisto
	build_dir: 'build',

	assets_dir: 'assets',

	// Javascript lähde- / kohdehakemisto
	js_dir: 'js',

	// Twig template-hakemisto
	templates_dir: 'templates',

	// Käännetty SASS -> CSS-tiedosto
	compiled_css: '<%= build_dir %>/assets/stylesheets/main.css',

	// Käännetty *.js => single JS-tiedosto
	compiled_js: '<%= build_dir %>/<%= assets_dir %>/js/code.js',

	compiled_vendor_js: '<%= build_dir %>/<%= assets_dir %>/js/vendor.js',

	// Vendor-hakemisto
	vendor_dir: 'vendor/frontend',

	// Lähdetiedostot
	sources: {
		js: [
			'<%= js_dir %>/*.js',
			'<%= js_dir %>/**/*.js'
		],
		twig: [ '<%= templates_dir %>/**/*.twig' ],
		wake_custom: [ 'custom/**/*.php' ],
		sass: [ 'sass/**/*.scss' ],
		base: '<%= templates_dir %>/base.twig',
		manifest: 'manifest*.xml' // Ei voi olla array, koska rsync
	},

	// Kaikki localet
	locale: [ 'locale/**/*' ],

	// Kaikki assetit
	assets: [ 'assets/**/*', '!assets/js/**/*' ],

	// Vendor-skriptit, jotka tarvitaan. Devauksessa ei minifoituja.
	// Nämä paketoidaan kaikki yhdeksi skriptiksi.
	// Huomaa, että järjestyksellä on väliä.
	vendor_dev: [
		'<%= vendor_dir %>/angular/angular.js',
		'<%= vendor_dir %>/angular-*/*.js',
		'!<%= vendor_dir %>/angular*/index.js',
		'!<%= vendor_dir %>/**/*.min.js',
		'!<%= vendor_dir %>/**/Gruntfile.js'
	],

	// Vendor-skriptit, jotka tarvitaan. Releaseen minifoidut.
	vendor_release: [
		'<%= vendor_dir %>/angular/angular.min.js',
		'<%= vendor_dir %>/angular-*/*.min.js'
	]
};




// GRUNT TASKIT
// ----------------------------------------------------------------------------------------------

module.exports = function (grunt) {

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-xml-validator');
	grunt.loadNpmTasks('grunt-rsync');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-sass-globbing');

	var _ = require('lodash');

	// Omat overridet.
	// Rsyncin konffi on normaalisti siellä.
	// grunt.overrides.js.example opastaa formaatin.

	var overrides;
	if(grunt.file.exists('./grunt.overrides.js')) {
		overrides = require('./grunt.overrides.js');
	}


	var taskConfig = {

		// Rsync-kopiointi.
		// Assetseille lokaali rsync, jotta kopioidaan vain muuttuneet assetit
		// build-hakemiston watchin aikana.
		rsync: {
			options: {
				args: ['--verbose'],
				exclude: ['.*'],
				recursive: true,
				compareMode: 'checksum',
				delete: true
			},
			assets: {
				options: {
					src: "assets/",
					dest: "<%= build_dir %>/assets/",
					exclude: ["*vendor*", "js/*", "stylesheets/*"]
				}
			},
			manifest: {
				options: {
					src: ['<%= sources.manifest %>'],
					dest: "<%= build_dir %>/"
				}
			},
			locale: {
				options: {
					src: "locale/",
					dest: "<%= build_dir %>/locale/"
				}
			},
			wake_custom: {
				options: {
					src: "custom/",
					dest: "<%= build_dir %>/custom/"
				}
			},
			twig: {
				options: {
					src: "templates/",
					dest: "<%= build_dir %>/templates/"
				}
			}
		},

		// SASS compile
		sass: {
			dev: {
				options: {
					sourcemap: 'auto',
					style: 'expanded'
				},
				files: {
					'<%= compiled_css %>': 'sass/main.scss'
				}
			},
			release: {
				options: {
					sourcemap: 'auto',
					style: 'compressed'
				},
				files: {
					'<%= compiled_css %>': 'sass/main.scss'
				}
			}
		},

		// Käytetään globbing-taskia, joka generoi "globbauksen" _globbing-tiedostoihin.
		sass_globbing: {
			generate_globs: {
				files: {
					'sass/_globbing-utility.scss': 'sass/utility/**/*.scss',
					'sass/partials/_globbing-base.scss': 'sass/partials/base/**/*.scss',
					'sass/partials/_globbing-layouts.scss': 'sass/partials/layouts/**/*.scss',
					'sass/partials/_globbing-blocks.scss': 'sass/partials/blocks/**/*.scss'
				},
				options: {
					useSingleQuotes: false
				}
			}
		},

		// Kokoaa scriptit yhteen, minifoi, jne.
		uglify: {
			release: {
				options: {
					mangle: {
						except: ['jQuery', 'angular']
					},
					compress: {
						drop_console: true
					},
					sourceMap: true,
					sourceMapIncludeSources: true
				},
				files: [
					{
						'<%= compiled_js %>': [
							'<%= sources.js %>'
						]
					}
				]
			},
			dev: {
				options: {
					mangle: false,
					beautify: true,
					comments: true,
					sourceMap: true,
					sourceMapIncludeSources: true
				},
				files: [
					{
						'<%= compiled_js %>': [
							'<%= sources.js %>'
						]
					}
				]
			},
			vendor_release: {
				options: {
					mangle: false,
					beautify: false,
					comments: false,
					sourceMap: false,
					sourceMapIncludeSources: false
				},
				files: [
					{
						'<%= compiled_vendor_js %>': [ '<%= vendor_release %>' ]
					}
				]
			},
			vendor_dev: {
				options: {
					mangle: false,
					beautify: true,
					comments: true,
					sourceMap: true,
					sourceMapIncludeSources: true
				},
				files: [
					{
						'<%= compiled_vendor_js %>': [ '<%= vendor_dev %>' ]
					}
				]
			}

		},

		// Validoi manifestin
		xml_validator: {
			manifest: {
				src: [ '<%= sources.manifest %>' ]
			}
		},

		// Javascriptin syntax-tarkastus
		jshint: {
			src: [
				'<%= sources.js %>'
			],
			gruntfile: [
				'Gruntfile.js'
			],
			options: {
				curly: false,
				immed: true,
				newcap: false,
				noarg: true,
				sub: true,
				boss: true,
				eqeqeq: false,
				eqnull: true,
				loopfunc: true,
				multistr: true,
				'-W001': true
			},
			globals: {
			}
		},

		// Minifoi käännetyn CSS:n
		cssmin: {
			minify: {
				files: {
					'<%= compiled_css %>' : ['<%= compiled_css %>']
				}
			}
		},

		// Autoprefixer
		autoprefixer: {
			default: {
				files: {
					'<%= compiled_css %>' : '<%= compiled_css %>'
				},
				options: {
					map: true
				}
			}
		},

		// delta == watch-taskin konfiguraatio
		delta: {
			options: {
				livereload: false
			},

			manifest: {
				files: ['<%= sources.manifest %>'],
				tasks: ['rsync:manifest']
			},

			js: {
				files: [ '<%= sources.js %>' ],
				tasks: [ 'jshint', 'uglify:dev' ]
			},

			sass: {
				files: [ '<%= sources.sass %>' ],
				tasks: [
					'sass_globbing',
					'sass:dev',
					'autoprefixer:default'
				]
			},

			vendor: {
				files: [ '<%= vendor_dir %>' ],
				tasks: [
					'uglify:vendor_dev'
				]
			},

			assets: {
				files: [ '<%= assets %>' ],
				tasks: [
					'rsync:assets'
				]
			},

			wake_custom: {
				files: ['<%= sources.wake_custom %>'],
				tasks: [
					'rsync:wake_custom'
				]
			},

			twig: {
				files: [ '<%= sources.twig %>' ],
				tasks: [
					'rsync:twig'
				]
			}

		}


	};

	_.merge(taskConfig, opts);
	if(overrides !== undefined) {
		_.merge(taskConfig, overrides.taskConfig);
	}

	grunt.initConfig(taskConfig);

	grunt.registerTask('build-dev', [
		'xml_validator:manifest',
		'sass_globbing',
		'sass:dev',
		'autoprefixer:default',
		'jshint',
		'uglify:vendor_dev',
		'uglify:dev',
		'rsync:assets',
		'rsync:wake_custom',
		'rsync:twig',
		'locale',
		'rsync:locale'
	]);

	grunt.registerTask('build-release', [
		'xml_validator:manifest',
		'rsync:manifest',	// Tärkeä olla ennen muita rsyncejä, koska tämä luo build-kansion, jos sitä ei ole
		'sass_globbing',
		'sass:release',
		'autoprefixer:default',
		'cssmin:minify',
		'jshint',
		'uglify:vendor_release',
		'uglify:release',
		'rsync:assets',
		'rsync:wake_custom',
		'rsync:twig',
		'locale',
		'rsync:locale'
	]);

	grunt.renameTask('watch', 'delta');
	grunt.registerTask('watch', ['build-dev', 'delta']);
	grunt.registerTask('default', ['watch']);

	// i18n stuff
	grunt.registerTask('locale', '', function () {
		grunt.log.writeln('Generating/updating locale files...');
		var execSync = require('sync-exec');
		execSync('npm run build-locales');
	});

};