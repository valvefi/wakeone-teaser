/**
 * Angular application configuration.
 */
(function() {

	var appDependencies = [
		'ngAnimate',
		'ngSanitize'
	];

	angular
		.module('app', appDependencies)
		.config(config)
		.run(run);

	config.$inject = ['$interpolateProvider'];

	function config($interpolateProvider) {
		$interpolateProvider.startSymbol('[[').endSymbol(']]');
	}

	function run() {
		console.log("Angular has been set up.");
	}

})();