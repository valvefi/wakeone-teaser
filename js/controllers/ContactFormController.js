(function() {

	angular
		.module('app')
		.controller('ContactFormController', contactFormController);
	
	contactFormController.$inject = ['$scope', '$window', '$http'];
	
	function contactFormController($scope, $window, $http) {


		$scope.formSubmitted = false;
		
		$scope.sendContactForm = function() {

			$scope.errors = {};
			if (!$scope.name) {
				$scope.errors.name = 'Syötä nimi';
			}

			if (!$scope.email) {
				$scope.errors.email = 'Virheellinen sähköpostiosoite';
			}

			if (!$scope.message) {
				$scope.errors.message = 'Viesti ei voi olla tyhjä';
			}

			if ( $scope.errors.message || $scope.errors.email || $scope.errors.name ) {
				return
			}

			var formData = {
				formID: 'contactForm',	
				name: $scope.name,
				email: $scope.email,
				phone: $scope.phone,
				message: $scope.message
			};
			if ($window.wakeTestEmail !== undefined) {
				formData.wakeTestEmail = $window.wakeTestEmail;
			}

			$http({
				method: "POST",
				url: "/api/1/form/submit",
				data: formData
			})
			.then(
				function() {
					$scope.formSubmitted = true;
				},
				function(error) {
					console.log(error)
					$scope.errors.general = 'Viestin lähettäminen epäonnistui'
				}
			);
			
		}
		
	}
})();

