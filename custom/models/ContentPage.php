<?php

namespace wakeoneTeaser\models;

use RCMS\Evention_CreatedModified;

class ContentPage extends \RCMS\ContentDocument
{

	/**
	 * Get previous and next article
	 *
	 * @return array
	 *
	 */
	public function getPrevNextArticle()
	{
		$data = array();
		$query = array(
			'className' => $this->getClassName(),
			'article' => true,
		);
		$created = $this->getAttribute(Evention_CreatedModified::CREATED);
		$prev = $this->findOne(array_merge($query, array(Evention_CreatedModified::CREATED => array('$lt' => $created))), false, array('created' => -1));
		if (!empty($prev))
		{
			$data['prev'] = $prev->getCanonicalUrl();
		}
		$next = $this->findOne(array_merge($query, array(Evention_CreatedModified::CREATED => array('$gt' => $created))), false, array('created' => 1));
		if (!empty($next))
		{
			$data['next'] = $next->getCanonicalUrl();
		}
		return $data;
	}


}

